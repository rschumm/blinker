#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#include <Adafruit_NeoPixel.h>


#define PIN 6
#define PIN_ONBOARD 8

#define orange strip.Color(255,100,0)
#define schwarz strip.Color(0,0,0)
#define remy strip.Color(66,161,255)

Adafruit_NeoPixel strip = Adafruit_NeoPixel(4, PIN, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip_onboard = Adafruit_NeoPixel(1, PIN_ONBOARD, NEO_GRB + NEO_KHZ800);

/* Assign a unique ID to this sensor at the same time */
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);


unsigned long  drehTimer = 0;

int ax = 0;
int ay = 0;
int az = 0;






class Blinker
{
    // Class Member Variables
    // These are initialized at startup
    unsigned long blinkerTimer = 0;
    bool blinkt = false;
    uint32_t aktuelleFarbe = 0;
    uint32_t blinkerFarbe;
    int kadenz;

    Adafruit_NeoPixel strip;

    // Constructor - creates a Flasher
    // and initializes the member variables and state
  public:
    Blinker(Adafruit_NeoPixel neo_strip, uint32_t farbe, int zeit)
    {
      strip = neo_strip;
      blinkerFarbe = farbe;
      kadenz = zeit;

      //strip.begin();
      //strip.show();

      uint32_t aktuelleFarbe = schwarz;
      blinkerTimer = 0;
    }

    void Begin() {
      strip.begin();
    }

    void Update()
    {
      unsigned long periode = millis() - blinkerTimer;
      //Serial.println(periode);
      if (periode < kadenz && blinkt && aktuelleFarbe != blinkerFarbe) {
        //Serial.println("blinkerFarbe");
        aktuelleFarbe = blinkerFarbe;
        alleInFarbe(blinkerFarbe);
      }
      if ((periode >= kadenz && aktuelleFarbe != schwarz) || !blinkt ) {
        //Serial.println("schwarz");
        aktuelleFarbe = schwarz;
        alleInFarbe(schwarz);

      }
      if (periode > kadenz * 2) {
        //Timer zurücksetzen
        blinkerTimer = millis();
      }
    }

    void toggleBlinker() {
      Serial.println("Toggle");
      blinkt = !blinkt;
    }

    void alleInFarbe(uint32_t pixelColor) {
      for (int i = 0; i < strip.numPixels(); i++) {
        strip.setPixelColor(i, pixelColor);
      }
      //strip.clear();
      strip.show();
    }


};



Blinker blinker1(strip, orange, 500);
Blinker blinker2(strip_onboard, remy, 200);


void setup() {

  Serial.begin(9600);


  blinker1.Begin();
  blinker2.Begin();
  blinker2.toggleBlinker();
  blinker1.toggleBlinker();

  /* Initialise the sensor */
  if (!accel.begin())
  {
    /* There was a problem detecting the ADXL345 ... check your connections */
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while (1);
  }


}

void loop() {



  blinker1.Update();
  blinker2.Update();


  if (checkEventSchlag()) {
    blinker1.toggleBlinker();
  }

/*

  if (checkEventRotation()) {
    blinker1.toggleBlinker();
  }
*/


}





/*Event der Velofahrer hat den Arm schnell gedreht */
//speichert alle 250ms die Richtung und liefert die Änerung seit der letzten Speicherung
bool checkEventRotation() {

  bool returnEvent = false;
  int periode = millis() - drehTimer;

  //Wert jetzt:
  sensors_event_t sensorevent;
  accel.getEvent(&sensorevent);
  float jx = sensorevent.magnetic.x;
  float jy = sensorevent.magnetic.y;
  float jz = sensorevent.magnetic.z;

  if (periode > 1000) {
    //Timer zurücksetzen:
    drehTimer = millis();
    //Wert alt speichern:
    Serial.println("Alter Wert gemerkt: ");
    ax = jx;
    ay = jy;
    az = jz;
  }


  float rotation = winkel(ax, ay, az, jx, jy, jz);

  //Serial.print("Winkel: "); Serial.println(rotation);
  if (rotation > 1.5) {
    Serial.print("Winkel Event: "); Serial.println(rotation);
    delay(100);
    ax = jx;
    ay = jy;
    az = jz;
    returnEvent = true;
  }

  //liefert im Zweifelsfall keinen Event
  return returnEvent;

}





/*Event der Velofahrer hat auf den Arm geschlagen*/
bool checkEventSchlag() {

  //delay(500);

  /* Get a new sensor event */
  sensors_event_t event;
  if (accel.getEvent(&event)) {

    //Serial.print("ev: ");
    //Serial.println(event);

    /* Display the results (acceleration is measured in m/s^2) */
    float a = beschleunigung(event.acceleration.x, event.acceleration.y, event.acceleration.z);
    //float a = event.acceleration.x;
    //float a = 1.0;
    Serial.print("a: "); Serial.println(a);


    if (a > 15.00) {
      Serial.print("Schlag Event: "); Serial.println(a);
      //delay(100);
      return true;
    } else {
      return false;
    }
  }

  return false;


}




/* Länge des Beschleunigungsvektors normiert auf Erdbeschleunigung */
float beschleunigung(float x, float y, float z) {
  return abs(laenge(x, y, z) - 9.81);
}

/* Länge des Beschleunigungsvektors normiert auf Erdbeschleunigung */
float laenge(float x, float y, float z) {
  return sqrt(x * x + y * y + z * z) ;
}

/* berechnet den Winkel in Radians zwischen zwei Vektoren a und b, 0 wenn einer (0,0,0) ist*/
float winkel(float a1, float a2, float a3, float b1, float b2, float b3) {
  if (laenge(a1, a2, a3) == 0 || laenge(b1, b2, b3) == 0) {
    return 0;
  }
  float skalar = a1 * b1 + a2 * b2 + a3 * b3;
  return acos( skalar / (laenge(a1, a3, a3) * laenge(b1, b2, b3)));
  //kann auch event.magnetic.heading benutzt werden?
}


